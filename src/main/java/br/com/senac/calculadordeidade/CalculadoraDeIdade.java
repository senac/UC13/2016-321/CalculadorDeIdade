package br.com.senac.calculadordeidade;

public class CalculadoraDeIdade {

    public static final String MAIOR_IDADE = "maior de Idade";
    public static final String MENOR_IDADE = "menor de Idade";
    public static final String IDOSO = "pessoa de Idosa";

    public String calcular(int idade) {

        if (idade > 65) {
            return IDOSO;
        } else if (idade >= 18) {
            return MAIOR_IDADE;
        } else {
            return MENOR_IDADE;
        }

    }
}
