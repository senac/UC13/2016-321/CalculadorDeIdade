
package br.com.senac.calculadordeidade;

import org.junit.Test;
import static org.junit.Assert.*;

public class CalculadoraDeIdadeTest {
    
    @Test
    public void deveSerMaiorDeIdade(){
        int idade = 21 ; 
        CalculadoraDeIdade calculadoraDeIdade = new CalculadoraDeIdade();
        String Resultado = calculadoraDeIdade.calcular(idade); 
        assertEquals(CalculadoraDeIdade.MAIOR_IDADE, Resultado);
        
    }
    
    
     @Test
    public void deveSerMenorDeIdade(){
        int idade = 17 ; 
        CalculadoraDeIdade calculadoraDeIdade = new CalculadoraDeIdade();
        String Resultado = calculadoraDeIdade.calcular(idade); 
        assertEquals(CalculadoraDeIdade.MENOR_IDADE, Resultado);
        
    }
    
     @Test
    public void deveSerIdoso(){
        int idade = 66 ; 
        CalculadoraDeIdade calculadoraDeIdade = new CalculadoraDeIdade();
        String Resultado = calculadoraDeIdade.calcular(idade); 
        assertEquals(CalculadoraDeIdade.IDOSO, Resultado);
        
    }
    
}
